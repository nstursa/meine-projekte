import 'package:flutter/material.dart';

class L10n {
  static const List<Locale> support = [
    Locale("de"),
    Locale("en"),
    Locale("pl")
  ];
}
