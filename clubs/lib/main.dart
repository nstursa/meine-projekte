import 'package:clubs/screens/club_detail_screen.dart';
import 'package:clubs/screens/club_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      /*    localeResolutionCallback: (locale, supportedLocales){
        for(var supportedLocale in supportedLocales) {
          if(supportedLocale.languageCode = locale!.languageCode && supportedLocale.countryCode = locale.countryCode != null) {
            return supportedLocale;
          }
        }
        return supportedLocales.first;
      },*/
      supportedLocales: const [
        Locale('de', ''),
        Locale('en', ''),
        Locale('pl', ''),
      ],
      //  onGenerateTitle: (BuildContext context) =>
      //      AppLocalizations.of(context).title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const ClubListScreen(),
      routes: {
        ClubListScreen.routeName: (context) => const ClubListScreen(),
        ClubDetailScreen.routeName: (context) => const ClubDetailScreen()
      },
    );
  }
}
