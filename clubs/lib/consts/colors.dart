import 'package:flutter/material.dart';

class ColorsConsts {
  static Color mainColor = const Color(0xFF01C13B);
  static Color white = const Color(0xFFFFFFFF);
  static Color whiteBackground = const Color(0xFFFFFFFF);
  static Color black = const Color(0xFF000000);
  static Color title = const Color(0xFF000000);
  static Color gray = const Color(0xFF9E9E9E);
  static Color darkgray = const Color(0xFF212121);
  static Color transparent = const Color(0x00000000);
}
