import 'package:clubs/consts/colors.dart';
import 'package:clubs/models/club.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:http/http.dart';
import 'dart:convert';

import 'club_detail_screen.dart';

class ClubListScreen extends StatefulWidget {
  static const String routeName = '/product-list';

  const ClubListScreen({Key? key}) : super(key: key);

  @override
  _ClubListScreenState createState() => _ClubListScreenState();
}

class _ClubListScreenState extends State<ClubListScreen> {
  late final String title;
  List<Club> clubs = [];
  int num = 1;

  final url = "https://public.allaboutapps.at/hiring/clubs.json";

//-------- Fetch all data from url-----------
  void fetchPosts() async {
    try {
      final response = await get(Uri.parse(url));
      final jsonData = jsonDecode(utf8.decode(response.bodyBytes)) as List;

      setState(() {
        clubs = jsonData.map((e) => Club.fromJson(e)).toList();
      });
    } catch (err) {
      //muss noch der ERROR abgefangen werden
    }
  }

//-------- Sort all data by NAME -----------
  Future<void> sortNameJsonFile() async {
    num = 2;

    final response = await get(Uri.parse(url));
    final jsonData = jsonDecode(utf8.decode(response.bodyBytes)) as List;

    setState(() {
      clubs = jsonData.map((e) => Club.fromJson(e)).toList();
      clubs.sort((a, b) {
        return a.name.toLowerCase().compareTo(b.name.toLowerCase());
      });
    });
  }

//-------- Sort all data by VALUE -----------
  Future<void> sortValueJsonFile() async {
    num = 1;

    final response = await get(Uri.parse(url));
    final jsonData = jsonDecode(utf8.decode(response.bodyBytes)) as List;

    setState(() {
      clubs = jsonData.map((e) => Club.fromJson(e)).toList();
      clubs.sort((a, b) => a.value.compareTo(b.value));
    });
  }

  @override
  void initState() {
    super.initState();
    fetchPosts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorsConsts.mainColor,
        title: const Text('all about clubs',
            style: TextStyle(color: Colors.white)),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.filter_list,
              color: ColorsConsts.white,
            ),
            onPressed: () {
              if (num == 1) {
                sortValueJsonFile();
                num++;
                // ignore: avoid_print
                print('+++++++++++++$num');
              } else {
                sortNameJsonFile();
                num--;
                // ignore: avoid_print
                print('+++++++++++++$num');
              }
            },
          ),
        ],
      ),
      body: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (clubs.isNotEmpty)
            Expanded(
              child: ListView.builder(
                itemCount: clubs.length,
                itemBuilder: (BuildContext context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed(
                          ClubDetailScreen.routeName,
                          arguments: jsonEncode(clubs[index]));
                    },
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Image.network(
                              clubs[index].image,
                              fit: BoxFit.contain,
                              width: 80,
                              height: 100,
                              alignment: Alignment.center,
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(20, 20, 0, 0),
                              child: SizedBox(
                                width: 280,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      clubs[index].name,
                                      style: TextStyle(
                                          color: ColorsConsts.title,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          clubs[index].country,
                                          style: const TextStyle(
                                            fontWeight: FontWeight.normal,
                                          ),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.fromLTRB(
                                              0, 16, 0, 0),
                                          child: Text(
                                            AppLocalizations.of(context)
                                                .millions(clubs[index]
                                                    .value
                                                    .toString()),
                                            style:
                                                const TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          else
            Text(
              AppLocalizations.of(context).noClubsText,
              style: const TextStyle(fontSize: 18),
            ),
        ],
      ),
    );
  }
}
