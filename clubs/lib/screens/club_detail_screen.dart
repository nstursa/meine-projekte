import 'dart:convert';

import 'package:clubs/consts/colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:clubs/models/club.dart';
import 'package:flutter/material.dart';

class ClubDetailScreen extends StatefulWidget {
  static const String routeName = '/product-detail';

  const ClubDetailScreen({Key? key}) : super(key: key);

  @override
  _ClubDetailScreenState createState() => _ClubDetailScreenState();
}

class _ClubDetailScreenState extends State<ClubDetailScreen> {
  var productName = "";
  var imag = "";
  // ignore: prefer_typing_uninitialized_variables
  var val;
  // ignore: prefer_typing_uninitialized_variables
  var wins;
  Club? club;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    var productString = ModalRoute.of(context)?.settings.arguments as String;
    // ignore: avoid_print
    print(productString);

    var productJson = jsonDecode(productString);
    // ignore: avoid_print
    print(productJson);

    setState(() {
      club = Club.fromJson(productJson);
      productName = club!.name;
      imag = club!.image;
      val = club!.value;
      wins = club!.europeantitles;
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorsConsts.mainColor,
          title: Text(productName, style: TextStyle(color: ColorsConsts.white)),
          actions: const <Widget>[],
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                width: double.infinity,
                height: 40.0,
                child: DecoratedBox(
                  decoration: BoxDecoration(color: ColorsConsts.darkgray),
                ),
              ),
              Container(
                width: double.infinity,
                height: 200,
                decoration: BoxDecoration(
                    color: ColorsConsts.darkgray,
                    image: DecorationImage(
                        image: NetworkImage(imag), fit: BoxFit.contain)),
              ),
              Container(
                width: double.infinity,
                child: Text(
                  club!.country.toString(),
                  style: TextStyle(
                      backgroundColor: ColorsConsts.darkgray,
                      color: ColorsConsts.white,
                      fontSize: 22,
                      fontWeight: FontWeight.bold),
                ),
                decoration: BoxDecoration(color: ColorsConsts.darkgray),
                padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(16, 20, 16, 0),
                child: Text(
                  AppLocalizations.of(context)
                      .detailText1(club!.name, club!.country, val.toString()),
                  style: const TextStyle(fontSize: 18),
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(16, 10, 20, 0),
                child: Text(
                  AppLocalizations.of(context)
                      .detailText2(club!.name, wins.toString()),
                  style: const TextStyle(fontSize: 18),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
