class Club {
  String id;
  String name;
  String country;
  int value;
  String image;
  int europeantitles;

  Club({
    this.id = "",
    this.name = "",
    this.country = "",
    this.value = 0,
    this.image = "",
    this.europeantitles = 0,
  });

  factory Club.fromJson(Map<String, dynamic> json) {
    return Club(
      id: json['id'],
      name: json['name'],
      country: json['country'],
      value: json['value'],
      image: json['image'],
      europeantitles: json['european_titles'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['country'] = country;
    data['value'] = value;
    data['image'] = image;
    data['european_titles'] = europeantitles;
    return data;
  }
}
